import Logic.IMailLogic;
import Logic.PickMail;
import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Tests {

    private IMailLogic gl;


    @Test
    public void UI(){
        this.gl = new PickMail()
                .setLoginAndPassword("some@gmail.com","thing");
        gl
                .login()
                .searchWithSubject("same word from subject")
                .validateDate(new SimpleDateFormat("d MMMMM yyyy", Locale.US).format(new Date()))
                .validateFrom("foo@gmail.com")
                .validateSubject("full subject")
                .validateText("same marker from letter text")
                .logOut();
    }

    @Test
    public void IMAP(){
        long counter =
                new PickMail()
                        .FastSearch("some@gmail.com","thing")
                        .setSubject("fullSubject")
                        .setFrom("foo@gmail.com")
                        .setDate(new SimpleDateFormat("d MMMMM yyyy", Locale.US).format(new Date()))
                        .setText("")
                        .count();
        Assert.assertTrue("Can't find anything",counter>0);
    }

}
