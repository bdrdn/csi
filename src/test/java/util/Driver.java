package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Driver {
    public WebDriver getDriver(String set_browser) {
        if (set_browser == null) {
            throw new IllegalArgumentException("set_browser can't be null");
        } else {
//            if (set_browser.equalsIgnoreCase("remote_chrome")) return remote_chrome();
//            if (set_browser.equalsIgnoreCase("remote_firefox")) return remote_firefox();
            if (set_browser.equalsIgnoreCase("chrome")) return chrom();
//            if (set_browser.equalsIgnoreCase("firefox")) return firefox();
            throw new IllegalArgumentException("Invalid browser name ".concat(set_browser));
        }
    }

    private WebDriver chrom() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--dns-prefetch-disable");
        options.addArguments("--start-maximized");
        System.setProperty("webdriver.chrome.driver", "./src/chromedriver.exe");
        return new ChromeDriver(options);
    }
}
