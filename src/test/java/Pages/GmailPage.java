package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GmailPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public GmailPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "input[type=\"email\"]")
    private WebElement emailInput;

    //    Скорее всего язык известен необходимости в таком нет.
    @FindBy(xpath = "//*[@role=\"button\"]//*[contains(text(),'Next')]|//*[@role=\"button\"]//*[contains(text(),'Далее')]")
    private WebElement nextButton;

    @FindBy(css = "input[type=\"password\"]")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@aria-label=\"Search\"]")
    private WebElement searchInput;

    @FindBy(xpath = "//*[not(ancestor::*[@role='tabpanel'])]/tbody[preceding-sibling::colgroup]/tr")
    private List<WebElement> lettersList;

    @FindBy(xpath = "//table[@role='presentation']//h2")
    private WebElement msgSubjectValue;

    //[]? Спасибо цепочкам писем
    @FindBy(xpath = "//*[@class='go']")
    private List<WebElement> msgFromFieldValue;

    @FindBy(xpath = "//*[@class=\"gs\"]/div[@class=\"\"]")
    private List<WebElement> msgTextValue;

    @FindBy(xpath = "//*[@role=\"listitem\"]//tbody//td[2]/div/span[2]")
    private List<WebElement> msgDateValue;

    public String getSubject() {
        return waitAndGetText(msgSubjectValue);
    }

    public String getFrom() {
        return waitAndGetText(msgFromFieldValue.get(0));
    }

    public String getMsgText() {
        return waitAndGetText(msgTextValue.get(0));
    }

    public String getDate() {
        waitElement(msgDateValue.get(0));
        return msgDateValue.get(0).getAttribute("title");
    }

    public GmailPage fillEmailInput(String email) {
        waitAndFill(emailInput, email);
        return this;
    }

    public GmailPage clickNextButton() {
        waitAndClick(nextButton);
        return this;
    }

    public GmailPage fillPasswordInput(String password) {
        waitAndFill(passwordInput, password);
        return this;
    }

    public GmailPage searchLetters(String conditions) {
        waitAndFill(searchInput, conditions + Keys.RETURN);
        try {
            Thread.sleep(500);
        } catch (Exception ignored) {
        }
        return this;
    }

    public GmailPage clickFirstLetterFromList() {
        waitElement(lettersList.get(0));
        // было бы хорошо знать какие вообще возможны subjects для приходящего письма но не зная можно и так
        if(lettersList.get(0).getText().toLowerCase().contains("no messages matched your search.")) {
            throw new AssertionError("can't find any msg with subject");
        }
        lettersList.get(0).click();
        return this;
    }

    public GmailPage clickCustomLetterFromList(int value) {
        if (lettersList.size() < value--) {
            throw new IllegalArgumentException("list of find letters contain just " + lettersList.size() + 1);
        }
        waitAndClick(lettersList.get(value));
        return this;
    }

    private void waitAndFill(WebElement element, String value) {
        waitElement(element);
        element.sendKeys(value);
    }

    private void waitAndClick(WebElement element) {
        waitElement(element);
        element.click();
    }

    private String waitAndGetText(WebElement element) {
        waitElement(element);
        return element.getText();
    }
//а можно было бы к каждому public написать по try{}catch{TimeoutException}
    private void waitElement(WebElement element) {
        wait.withMessage("no such element "+element.getTagName()+" on page "+driver.getCurrentUrl()).until(driver -> element.isDisplayed());
    }
}
