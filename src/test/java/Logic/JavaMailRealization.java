package Logic;

import com.sun.mail.imap.IMAPFolder;

import javax.mail.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class JavaMailRealization {

    private String login, password, imap_host;
    private String date, subject, text, from;

    JavaMailRealization(String login, String password, String imap_host) {
        this.imap_host = imap_host;
        this.login = login;
        this.password = password;
    }

    public JavaMailRealization setDate(String date) {
        this.date = date;
        return this;
    }

    public JavaMailRealization setFrom(String from) {
        this.from = from;
        return this;
    }


    public JavaMailRealization setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public JavaMailRealization setText(String text) {
        this.text = text;
        return this;
    }

    public long count() {
        try {
            return innerSearch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private long innerSearch() throws MessagingException{
        IMAPFolder folder = null;
        Properties props = System.getProperties();
        Session session = Session.getDefaultInstance(props, null);
        props.setProperty("mail.store.protocol", "imaps");
        try (Store store = session.getStore("imaps")) {
            store.connect(imap_host, login, password);
            folder = (IMAPFolder) store.getFolder("inbox");
            if (!folder.isOpen())
                folder.open(Folder.READ_WRITE);
            Message[] msg = folder.getMessages();
            return Arrays.stream(msg)
                    .filter(this::getText)
                    .filter(this::getSubject)
                    .filter(this::getFrom)
                    .filter(this::getDate)
                    .count();
//                    .max(Comparator.comparing(this::getSendDate))
//                    .map(this::getContent)
//                    .orElseThrow(NoSuchElementException::new);

        } finally {
            if (folder != null && folder.isOpen()) {
                folder.close(true);
            }
        }
    }

    private Date getSendDate(Message message) {
        try {
            return message.getSentDate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date(0);
    }


    private String getContent(Message message) {
        String s = "";
        try {
            if (message.getContentType().toLowerCase().contains("multipart")) {
                Multipart ss = (Multipart) message.getContent();
                for (int i = 0; i < ss.getCount(); i++) {
                    BodyPart bodyPart = ss.getBodyPart(i);
                    if (bodyPart.isMimeType("text/*")) {
                        s = (String) bodyPart.getContent();
                    }
                }
            } else {
                s = message.getContent().toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    private boolean getText(Message message) {
        try {
            if (text != null) return message.getContent().toString().contains(text);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private boolean getDate(Message message) {
        try {
            String d = new SimpleDateFormat("d MMMMM yyyy", Locale.US).format(message.getReceivedDate());
            if (date != null) return d.equals(date);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean getFrom(Message message) {
        if (from != null) {
            try {
                String f = message.getFrom()[0].toString();
                if (f.contains("<")) return f.split("<")[1].split(">")[0].equals(from);
                return f.equals(from);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private boolean getSubject(Message message) {
        try {
            if (subject != null) return message.getSubject().equals(subject);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
