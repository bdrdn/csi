package Logic;

import Pages.GmailPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class GmailLogic implements IMailLogic {

    private String password, login;
    private WebDriver driver;
    private GmailPage mp;

    GmailLogic(String email, String password, WebDriver driver) {
        this.driver = driver;
        this.password = password;
        this.login = email;
        this.mp = new GmailPage(driver);
    }

    @Override
    public IMailLogic login() {
        driver.get("https://accounts.google.com/");
        mp
                .fillEmailInput(this.login)
                .clickNextButton()
                .fillPasswordInput(this.password)
                .clickNextButton();
        try {
            Thread.sleep(500);
        } catch (Exception ignored) {
        }
        if (!driver.getCurrentUrl().contains("mail.google.com/mail/"))
            driver.get("https://mail.google.com/mail/#inbox");
        return this;
    }


    @Override
    public IMailLogic searchWithSubject(String atLeastOneFullWord) {
        mp.searchLetters(atLeastOneFullWord).clickFirstLetterFromList();
        return this;
    }

    @Override
    public IMailLogic validateDate(String someData) {
        Assert.assertEquals("Wrong date at msg", someData, mp.getDate().split(" at")[0]);
        return this;
    }

    @Override
    public IMailLogic validateFrom(String fromEmail) {
        String f = mp.getFrom().replace("<", "").replace(">", "");
        Assert.assertEquals("wrong from email", fromEmail, f);
        return this;
    }

    @Override
    public IMailLogic validateSubject(String subject) {
        Assert.assertEquals("Wrong subject", subject.trim(), mp.getSubject().trim());
        return this;
    }

    @Override
    public IMailLogic validateText(String someMarker) {
        Assert.assertTrue("msg doesn't contains marker", mp.getMsgText().contains(someMarker));
        return this;
    }

    @Override
    public void logOut() {
        driver.get("https://accounts.google.com/Logout");
    }
}
