package Logic;

public interface IMailLogic {
    IMailLogic login();
    IMailLogic searchWithSubject(String subject);
    IMailLogic validateDate(String someData);
    IMailLogic validateFrom(String from);
    IMailLogic validateText(String someMarker);
    IMailLogic validateSubject(String subject);
    void logOut();
}
