package Logic;

import org.openqa.selenium.WebDriver;
import util.Driver;

public class PickMail {

    public IMailLogic setLoginAndPassword(String login, String password){
        WebDriver driver = new Driver().getDriver("chrome");
        if(!login.contains("@")) throw new IllegalArgumentException("Login must be email (must contain '@')");
        switch (login.split("@")[1]){
            case "gmail.com":
                return new GmailLogic(login,password,driver);
//            case"someMail":
//                return new SomeMailLogic(login, password, driver);
            default:
                throw new IllegalArgumentException("haven't realisation for this mailService");
        }
    }

    public JavaMailRealization FastSearch(String login, String password){
        if(!login.contains("@")) throw new IllegalArgumentException("Login must be email (must contain '@')");
        switch (login.split("@")[1]){
            case "gmail.com":
                return new JavaMailRealization(login,password,"imap.googlemail.com");
//            case"someoverMail":
//                return new JavaMailRealization(login, password, "someserverimap");
            default:
                throw new IllegalArgumentException("haven't realisation for this mailService");
        }

    }
}
